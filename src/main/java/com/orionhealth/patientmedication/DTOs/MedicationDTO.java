package com.orionhealth.patientmedication.DTOs;

import com.orionhealth.patientmedication.model.Patient;

public class MedicationDTO {
	private Long id;
	private Long medicationDateTime;
	private String name;
	private String patientName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMedicationDateTime() {
		return medicationDateTime;
	}

	public void setMedicationDateTime(Long medicationDateTime) {
		this.medicationDateTime = medicationDateTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

}
