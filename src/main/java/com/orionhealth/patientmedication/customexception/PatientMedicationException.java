package com.orionhealth.patientmedication.customexception;

public class PatientMedicationException extends Exception {
	private static final long serialVersionUID = 2210668276247957386L;

	public PatientMedicationException(String message) {
		super(message);
	}

	public PatientMedicationException(String message, Throwable e) {
		super(message, e);
	}
}
