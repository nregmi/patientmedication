package com.orionhealth.patientmedication.service;

import com.orionhealth.patientmedication.customexception.PatientMedicationException;
import com.orionhealth.patientmedication.model.Patient;

public interface PatientService {
	void save(Patient patient) throws PatientMedicationException;

	Patient findByUsername(String username);
}
