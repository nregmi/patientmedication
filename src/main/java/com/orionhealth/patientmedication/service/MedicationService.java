package com.orionhealth.patientmedication.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.orionhealth.patientmedication.DTOs.MedicationDTO;
import com.orionhealth.patientmedication.DTOs.MedicationFilterDTO;
import com.orionhealth.patientmedication.model.Medication;

public interface MedicationService {
	void save(Medication mediation);

	Page<MedicationDTO> get(MedicationFilterDTO medicationFilterParam);
}
