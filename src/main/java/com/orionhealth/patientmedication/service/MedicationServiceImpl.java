package com.orionhealth.patientmedication.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.orionhealth.patientmedication.DTOs.MedicationDTO;
import com.orionhealth.patientmedication.DTOs.MedicationFilterDTO;
import com.orionhealth.patientmedication.model.Medication;
import com.orionhealth.patientmedication.model.Patient;
import com.orionhealth.patientmedication.repository.MedicationRepository;

@Service
public class MedicationServiceImpl implements MedicationService {

	@Autowired
	MedicationRepository medicationRepository;

	@Autowired
	PatientService patientService;

	@Override
	public void save(Medication medication) {
		String userDetails = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Patient patient = patientService.findByUsername(userDetails);
		medication.setPatient(patient);
		medicationRepository.save(medication);
	}

	@Override
	public Page<MedicationDTO> get(MedicationFilterDTO medicationFilterParam) {
		Timestamp fromTimeStamp = new Timestamp(medicationFilterParam.getFromDate());
		Timestamp toTimeStamp = new Timestamp(
				medicationFilterParam.getToDate() != null ? medicationFilterParam.getToDate()
						: System.currentTimeMillis());

		Pageable page = PageRequest.of(medicationFilterParam.getOffset(), medicationFilterParam.getSize(),
				Sort.by("medicationDateTime").descending());

		String userDetails = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Patient patient = patientService.findByUsername(userDetails);

		Page<Medication> pages = null;
		if (medicationFilterParam.getName() != null && !"".equals(medicationFilterParam.getName())) {
			pages = medicationRepository.filterByDateAndName(fromTimeStamp, toTimeStamp,
					medicationFilterParam.getName(), patient.getId(), page);
		} else if (fromTimeStamp != null && toTimeStamp != null){
			pages = medicationRepository.filterByFromAndToDate(fromTimeStamp, toTimeStamp, patient.getId(), page);
		} else {
			pages = medicationRepository.filterBySizeAndOffset(patient.getId(), page);
		}
		return new PageImpl<>(mapToMedicationDto(pages.getContent()), page,
				pages.getTotalElements());
	}

	private List<MedicationDTO> mapToMedicationDto(List<Medication> results) {
		List<MedicationDTO> medications = new ArrayList<MedicationDTO>();
		results.forEach(med -> {
			MedicationDTO medication = new MedicationDTO();
			medication.setId(med.getId());
			medication.setName(med.getName());
			medication.setMedicationDateTime(Timestamp.valueOf(med.getMedicationDateTime().toString()).getTime());
			medication.setPatientName(med.getPatient().getUsername());
			medications.add(medication);
		});
		return medications;
	}

}
