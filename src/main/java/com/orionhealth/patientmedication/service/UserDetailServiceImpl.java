package com.orionhealth.patientmedication.service;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.orionhealth.patientmedication.model.Patient;
import com.orionhealth.patientmedication.repository.PatientRepository;

@Service
public class UserDetailServiceImpl implements UserDetailsService{
	@Autowired
	PatientRepository patientRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Patient user = patientRepository.findByUsername(username);
        if (user == null) throw new UsernameNotFoundException(username);

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthorities());
	}
	
    public Collection<? extends GrantedAuthority> getAuthorities() {        
        return new HashSet<GrantedAuthority>();
    }
}
