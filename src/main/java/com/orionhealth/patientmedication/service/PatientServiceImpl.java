package com.orionhealth.patientmedication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.orionhealth.patientmedication.customexception.PatientMedicationException;
import com.orionhealth.patientmedication.model.Patient;
import com.orionhealth.patientmedication.repository.PatientRepository;

@Service
public class PatientServiceImpl implements PatientService {
	@Autowired
	PatientRepository patientRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public void save(Patient patient) throws PatientMedicationException {
//		if (patient.getUsername() == null || "".equals(patient.getUsername().trim())) {
//			throw new PatientMedicationException("Username must be provided.");
//		}
		patient.setPassword(bCryptPasswordEncoder.encode(patient.getPassword()));
		patientRepository.save(patient);

	}

	@Override
	public Patient findByUsername(String username) {
		return patientRepository.findByUsername(username);
	}

}
