package com.orionhealth.patientmedication.repository;

import com.orionhealth.patientmedication.model.Medication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;

public interface MedicationRepository extends JpaRepository<Medication, Long> {

    @Query("select m from Medication m WHERE m.patient.id= :userId")
    Page<Medication> filterBySizeAndOffset(@Param("userId") Long userId, Pageable pageRequest);

    @Query("select m from Medication m WHERE m.patient.id= :userId AND m.medicationDateTime BETWEEN :fromTime AND :endTime")
    Page<Medication> filterByFromAndToDate(@Param("fromTime") Timestamp fromTime, @Param("endTime") Timestamp endTime,
                                           @Param("userId") Long userId, Pageable pageRequest);

    @Query("select m from Medication m WHERE m.patient.id= :userId AND m.name = :medicationName AND m.medicationDateTime BETWEEN :fromTime AND :endTime")
    Page<Medication> filterByDateAndName(@Param("fromTime") Timestamp fromTime, @Param("endTime") Timestamp endTime,
                                         @Param("medicationName") String medicationName, @Param("userId") Long userId, Pageable pageRequest);

}
