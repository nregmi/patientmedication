package com.orionhealth.patientmedication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orionhealth.patientmedication.model.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {
	Patient findByUsername(String username);
}
