package com.orionhealth.patientmedication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.orionhealth.patientmedication.model.Medication;
import com.orionhealth.patientmedication.model.Patient;
import com.orionhealth.patientmedication.service.MedicationService;
import com.orionhealth.patientmedication.service.PatientService;

@RestController
@RequestMapping("/api/patient")
@Validated
public class PatientController {
	@Autowired
	PatientService patientService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public void registerPatient(@Valid @RequestBody Patient patient) throws Exception {
		patientService.save(patient);
	}
}
