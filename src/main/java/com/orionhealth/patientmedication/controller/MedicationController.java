package com.orionhealth.patientmedication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.orionhealth.patientmedication.DTOs.MedicationDTO;
import com.orionhealth.patientmedication.DTOs.MedicationFilterDTO;
import com.orionhealth.patientmedication.model.Medication;
import com.orionhealth.patientmedication.service.MedicationService;

@RestController
@RequestMapping("/api/medication")
public class MedicationController {
	@Autowired
	MedicationService medicationService;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addMedication(@RequestBody Medication medication) throws Exception {
		medicationService.save(medication);
	}

	@RequestMapping(value = "/get", method = RequestMethod.POST)
	public Page<MedicationDTO> getMedication(@RequestBody MedicationFilterDTO MedicationFilterDTO) throws Exception {
		return medicationService.get(MedicationFilterDTO);
	}

}
