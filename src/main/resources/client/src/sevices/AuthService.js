import Request, { setAuthorizationHeader } from './Request.js';
import { SIGN_UP_URL, LOGIN_URL } from "../constants.js";
import TokenService from "./TokenService";

export async function login(credential) {
    try {
        const { data: { token } } =  await Request.post(LOGIN_URL, credential);
        setAuthorizationHeader(token);
        TokenService.set(token);
        return true;
    } catch (e) {
        console.dir(e);
        return false;
    }
}

export async function register(payload) {
    try {
        await Request.post(SIGN_UP_URL, payload);

        return true;
    } catch (e) {
        console.dir(e);
        return false;
    }
}

export default {
    login,
    register,
}


