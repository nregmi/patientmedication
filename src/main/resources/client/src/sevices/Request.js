import axios from "axios";
import { BASE_URL } from "../constants.js";
import TokenService from "./TokenService";
import router from "../routes";

const request = axios.create({
    baseURL: BASE_URL,
})

export function setAuthorizationHeader(token) {
    request.defaults.headers.common.Authorization = `Bearer ${token}`;
}

export function removeAuthorizationHeader() {
    delete request.defaults.headers.common.Authorization;
}

request.interceptors.response.use(function (response) {
    return response;
},function (error) {
    if (error.response.status === 401) {
        removeAuthorizationHeader();
        TokenService.remove();

        router.push({ name: 'Login' });
    }

    return Promise.reject(error);
})

export default request;
