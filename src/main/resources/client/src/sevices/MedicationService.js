import Request from './Request.js';
import {MEDICATION_ADD_URL, MEDICATION_GET_URL} from "../constants.js";

export async function add(medication) {
    const response = await Request.post(MEDICATION_ADD_URL, medication);

    console.dir(response);
}

export async function search(filterParam) {
    return await Request.post(MEDICATION_GET_URL, filterParam);
}

export default {
    add,
    search
}
