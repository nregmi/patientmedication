import {setAuthorizationHeader} from "./Request";

const Storage = window.sessionStorage || window.localStorage;

export function has() {
    return !!Storage.getItem('token');
}

export function get() {
    return Storage.getItem('token')
}

export function set(token) {
    Storage.setItem('token', token);
}

export function remove() {
    Storage.clear();
}

export function initialize() {
    if (has()) {
        const token = get();
        setAuthorizationHeader(token);
    }
}

export default {
    has,
    get,
    set,
    remove,
    initialize,
};
