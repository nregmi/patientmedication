export const BASE_URL = process.env.NODE_ENV === 'production'
    ? '/api/'
    : 'http://localhost:8080/api/';

export const SIGN_UP_URL = "/patient/register";
export const LOGIN_URL = "/patient/login";
export const MEDICATION_GET_URL = '/medication/get';
export const MEDICATION_ADD_URL = '/medication/add';
