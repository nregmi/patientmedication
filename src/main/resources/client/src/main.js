import 'element-plus/packages/theme-chalk/src/index.scss';
import {createApp} from 'vue';
import router from './routes.js';
import App from './App.vue'
import './assets/scss/app.scss';
import ElementPlus from 'element-plus';

createApp(App)
    .use(router)
    .use(ElementPlus)
    .mount('#app')
