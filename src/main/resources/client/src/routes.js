import { createRouter, createWebHashHistory } from "vue-router";
import Login from "./views/auth/Login.vue";
import Register from "./views/auth/Register.vue";
import TokenService from "./sevices/TokenService.js";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import("./views/Home.vue"),
        meta: {
            authGuard: true,
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

const PUBLIC_ROUTES = ['Login', 'Register'];

router.beforeEach((to, from, next) => {
    console.log(to);
    console.log(TokenService.has() && PUBLIC_ROUTES.includes(to.name))

    if (to.meta.authGuard && !TokenService.has()) {
        return next({ name: 'Login' });
    }

    if (!to.meta.authGuard && TokenService.has()) {
        return next({ name: 'Home' });
    }

    return next();
});

export default router;
